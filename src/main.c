#include <zephyr/device.h>
#include <zephyr/drivers/lora.h>
#include <errno.h>
#include <zephyr/sys/util.h>
#include <zephyr/kernel.h>

int main(void)
{
	printk("Welcome to hello_lora_send\n");
	printk("Wait 5 seconds...\n");
	k_msleep(5000);
	
	const struct device *const lora_dev = DEVICE_DT_GET(DT_ALIAS(lora0));
	if (!device_is_ready(lora_dev)) {
		printk("%s Device not ready", lora_dev->name);
		return 0;
	}

	printk("configure lora\n");
	struct lora_modem_config config;
	config.frequency = 865100000;
	config.bandwidth = BW_125_KHZ;
	config.datarate = SF_8;
	config.preamble_len = 8;
	config.coding_rate = CR_4_5;
	config.iq_inverted = false;
	config.public_network = false;
	config.tx_power = 4;
	config.tx = true;

	if(lora_config(lora_dev, &config)< 0) {
		printk("Lora configuration failed... program termination\n");
		return 0;
	}

	printk("create payload\n");
	uint8_t data_len=10;
	char data[10];
	data[0]='h';
	data[1]='e';
	data[2]='l';
	data[3]='l';
	data[4]='o';
	data[5]='w';
	data[6]='o';
	data[7]='r';
	data[8]='l';
	data[9]='d';

	printk("loop\n");
	while (1) {
		printk("send\n");
		if(lora_send(lora_dev, data, data_len)<0) {
			printk("Lora send failed\n");
		}else{
			printk("Data sent\n");
		}
		k_msleep(10000);
	}
	return 0;
}
